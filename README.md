Vatsim-SSO

Easy way to use the VATSIM SSO system with Python applications. This will work with anything in python (web frameworks, command line tools and GUIs)

Fairly easy to install:
```
pip install vatsim-sso
```
OR
```
easy_install vatsim-sso
```
Dependencies will be pulled at the same time as well.

The wrapper follows the defaults for oauth_allow_suspended and oauth_allow_inactive which is 0. This can be overridden when initializing VatsimSSO by adding oauth_allow_inactive=1 and/or oauth_allow_suspended=1 as keyword arguments. At this time signature_method must be RSA_SHA1 the callback_uri should follow what is listed in the SSO system for your website. rsa_location should be the absolute path of the private key file.

To generate the key files you can run the following commands (linux systems).

```
openssl genrsa -out keyname.pem 2048
```

```
openssl rsa -in keyname.pem -pubout > keyname.pub
```

The above commands will give you a privatekey named keyname.pem and then a public key called keyname.pub


How to Use:

Initial Auth Token

```
from vatsim_sso import VatsimSSO
request = VatsimSSO(consumer_key='', consumer_secret='',
                    callback_uri='http://127.0.0.1:8000/vatsim/sso/step2',
                    rsa_location='key.pem', signature_method='RSA-SHA1').login_token()
```

The above code will return the auth token used when directing the user to https://cert.vatsim.net/sso/auth/pre_login/. As an example in django you would do the following to redirect them.
```
return HttpResponseRedirect('https://cert.vatsim.net/sso/auth/pre_login/?oauth_token=%s' % request)
```

Login Return

```
request = VatsimSSO(consumer_key='', consumer_secret='',
                     rsa_location='key.pem', signature_method='RSA-SHA1',
                     token=oauth_token, verifier=oauth_verifier).login_return()
```

The above code will return a json object with the information of the VATSIM member that has been authenticated.

If there are any questions feel free to post on the forum thread or an issue can be opened.